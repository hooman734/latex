\contentsline {chapter}{\hbox to\@tempdima {\hfil }Abstract}{iii}
\contentsline {chapter}{\hbox to\@tempdima {\hfil }Declaration}{iv}
\contentsline {chapter}{\hbox to\@tempdima {\hfil }Acknowledgements}{v}
\contentsline {chapter}{\numberline {1}Motivation}{1}
\contentsline {chapter}{\numberline {2}General \& Specific Goals}{3}
\contentsline {section}{\numberline {2.1}Notation}{3}
\contentsline {section}{\numberline {2.2}The Existence and Uniqueness of the Continuous Problem}{4}
\contentsline {chapter}{\numberline {3}Hypothesis}{6}
\contentsline {section}{\numberline {3.1}Notations}{6}
\contentsline {chapter}{\numberline {4}Methodology}{7}
\contentsline {section}{\numberline {4.1}Scheme 1}{7}
\contentsline {subsection}{\numberline {4.1.1}Methodology Sub}{7}
\contentsline {chapter}{\numberline {5}Numerical Experiments}{9}
\contentsline {section}{\numberline {5.1}Practical Algorithms}{9}
\contentsline {subsection}{\numberline {5.1.1}Iterative Method for Scheme 1 }{9}
\contentsline {subsubsection}{Concluding Remarks}{10}
\contentsline {chapter}{\numberline {6}Conclusions}{11}
\contentsline {chapter}{\hbox to\@tempdima {\hfil }Bibliography}{12}
\contentsline {chapter}{\hbox to\@tempdima {\hfil }Appendix}{13}
\contentsline {chapter}{\numberline {A}Basic and Auxiliary Results}{13}
\contentsline {section}{\numberline {A.1}Basic Results}{13}
