\contentsline {section}{\numberline {1}Inauguration}{2}
\contentsline {subsection}{\numberline {1.1}Motivation}{2}
\contentsline {subsection}{\numberline {1.2}Goal}{2}
\contentsline {subsection}{\numberline {1.3}Hypothesis}{3}
\contentsline {subsection}{\numberline {1.4}Methodology}{4}
\contentsline {subsubsection}{\numberline {1.4.1}Track the rate of downloading the Application}{5}
\contentsline {subsubsection}{\numberline {1.4.2}App Rating and App Commenting}{5}
\contentsline {subsubsection}{\numberline {1.4.3}Watching and monitoring the data transmission between all clients and of the servers}{5}
\contentsline {subsubsection}{\numberline {1.4.4}With the service of bus operating companies}{6}
\contentsline {subsubsection}{\numberline {1.4.5}With the cooperation of Crowd-Participants}{6}
\contentsline {subsubsection}{\numberline {1.4.6}Draw the Eye with Color}{6}
\contentsline {subsubsection}{\numberline {1.4.7}Use Icons to Guide Users}{6}
\contentsline {subsubsection}{\numberline {1.4.8}Ensure Device Compatibility}{7}
\contentsline {subsubsection}{\numberline {1.4.9}Offer Non-Obtrusive In-App Purchases}{7}
\contentsline {subsection}{\numberline {1.5}Literature review}{8}
