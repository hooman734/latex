\contentsline {part}{I\hspace {1em}Questions I made before starting to search}{2}
\contentsline {section}{\numberline {1}How to measure the success of an application}{2}
\contentsline {subsection}{\numberline {1.1}Pursue the rate of downloading the Application }{2}
\contentsline {subsection}{\numberline {1.2}Rate and Comments}{2}
\contentsline {subsection}{\numberline {1.3}Monitoring the data traffic of the servers}{2}
\contentsline {section}{\numberline {2}How to gather all needed information}{3}
\contentsline {subsection}{\numberline {2.1}With the service of bus operating companies}{3}
\contentsline {subsection}{\numberline {2.2}With the cooperation of Crowd-Participants}{3}
\contentsline {section}{\numberline {3}How to improve and make an application more user friendly}{3}
\contentsline {section}{\numberline {4}What are the best techniques to promote an application }{3}
\contentsline {part}{II\hspace {1em}Search strings I used to retrieve documents}{3}
\contentsline {part}{III\hspace {1em}The criteria I used for filtering }{4}
\contentsline {part}{IV\hspace {1em}Literature review}{5}
\contentsline {part}{V\hspace {1em}List of 10 papers selected I think are the most useful}{12}
